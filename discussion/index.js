// Javascript Statements:
	/*
		>> set of instructions that we tell the computer/ machine to perform
		>> JS statements usually ends with semicolon (;), to locate where the statements end
	*/

console.log("Hello once more");

// Comments
//- one-line comment <ctrl + /)
/* multi-line comment (ctrl + shift + / */
/* 
	>> Comments are parts of the code that gets ignored by the language
	>> Comments are meant to describe the written code
*/

// Variables
/*
	Syntax:
		let / const variableName;
*/

// Initialize Variables
 let hello;
 console.log(hello); //result: undefined

 // console.log(x); //result: error
 // let x;

// Declaration Variables
let firstName = "Jin"; //good variable name
let pokemon = 25000; //bad variable name

let FirstName = "Izuku"; // bad variable name
let firstNameAgain = "All Might"; //good variable name

// let first name = "Midoriya"; //bad variable name

//camelcase
	//lastName, emailAddress, mobileNumber
//underscores
let product_description = "lorem ipsum"
let product_id = "250000ea1000";

/*
	Syntax:
		let / const variableName = value;
		const- constant
*/
let productName = "desktop computer";
console.log(productName); //result: desktop computer

let productPrice = 18999;
console.log(productPrice); //result: 18999

const pi = 3.14;
console.log(pi); //result: 3.14

// Reassigning variable
/*
	Syntax:
		variableName = value
*/
productName = "Laptop";
console.log(productName); //result: Laptop

let friend = "Kate";
friend = "Chance";
console.log(friend); //result: Chance

// let friend = "Jane";
// console.log(friend); //result: error "identifier friend has already been declared"

// pi = 3.1;
// console.log(pi); //result: error "assignment to constant variable"

// Reassigning vs. Initializing variable 


// initialize variable
let supplier;
supplier = "Jane Smith Tradings";
console.log(supplier); //result: Jane Smith Tradings

// reassignment
supplier = "Zuitt Store";
console.log(supplier); //result: Zuitt Store

// var vs let/const
a = 5;
console.log(a); //result: 5
var a;

// b = 6;
// console.log(b); //result: 
// let b;

// let/ const local/ global scope

let outerVariable = 'hello';

{
	let innervariable = 'hello again';
}

console.log(outerVariable); // result: hello
// console.log(innerVariable); // result: innerVariable is not defined

// Multiple Variable Declaration
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand); //result: DC017, Dell

// // let is a reserved keyword
// const let = 'hello';
// console.log(let); //result: error

// Data Types
// String

let country = 'Philippines';
let city = "Manila City";

console.log(city, country); //result: Manila City Philippines

// Concatenating Strings
let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress); //result: Manila City, Philippines

// Mini-activity

let myName = 'Carl Joseph Ong'

let greeting = 'Hi I am ' + myName;
console.log(greeting);

// escape character (\)
	// "\n"- creates a new line in between texts

let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress);

// Numbers
// Integers/ Whole Numbers
let headcount = 26;
console.log(headcount); //result: 26

// Decimal Numbers
let grade = 74.9;
console.log(grade); //result: 74.9

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance); //result: 20000000000

// Combining text and strings
console.log("John's grade kast quarter is " + grade);

// Boolean
let isMarried = false;
let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

// Arrays
/*
	Syntax:
		let/const arrayName = [elementA, elementB, ....];
*/
// similar data types
let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BNHA", "AOT", "SxF", "KNY"];
console.log(anime);

// different data types
let random = ["JK", 24, true];
console.log(random);

// Objects Data Type
/*
	Syntex:
	let/const objectName = { propertyA : valueA, propertyB : valueB};
*/

let person = {
	fullName: "Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}
console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9
};
console.log(myGrades);

// typeof operator
console.log(typeof myGrades); //result: object
console.log(typeof grades); //result: object (special type of object)
console.log(typeof grade); //result: number

// anime = ['One Punch Man'];
// console.log(anime); //result: error (assignment to constant variable)

anime[0]= ['One Punch Man'];
console.log(anime);

// Null
let spouse = null;
console.log(spouse); //result: null
let zero = 0;
let emptyString = '';

// Undefined
let y;
console.log(y); //result: undefined